# python-plot-examples

A couple of plots to show what can easily be done in python using matplotlib and/or seaborn. 

Joint work by Thomas Dürrmeier, thomas.duerrmeier@edu.hefr.ch, and Mattias Dürrmeier, mattias.duerrmeier@unifr.ch.